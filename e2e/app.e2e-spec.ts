import { AgendarestPage } from './app.po';

describe('agendarest App', () => {
  let page: AgendarestPage;

  beforeEach(() => {
    page = new AgendarestPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
