import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http'

@Injectable()
export class ContactoService {

  contactos : any[] = [];

  constructor(private http : Http) { 
    this.listarContactos();
  }

  listarContactos(){
    this.contactos = [];
    this.http.get('http://localhost:8080/agendarest/contactos')
    .subscribe((res:Response) =>{
      let contacto = res.json();
      contacto.forEach(element => {
        this.contactos.push(element);
      });
    })
  }

  getContactos(): any[]{
    return this.contactos;
  }
}
