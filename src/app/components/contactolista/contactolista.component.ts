import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../../services/contacto.service';

@Component({
  selector: 'app-contactolista',
  templateUrl: './contactolista.component.html',
  styleUrls: ['./contactolista.component.css']
})
export class ContactolistaComponent implements OnInit {

  constructor(private contactoservice : ContactoService) { }

  ngOnInit() {
  }

}
